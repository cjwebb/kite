# Kite

A modern version control system, built for teams.

## High-level Design - Prototype

A central server propagating changes from/to clients. The server is a single-point of failure.
Clients configure which vcs roots they want to send/receive changes for.
kite-client is a convenient tool used to connect to the change-stream.

## Unsolved Problems with the prototype
- How do these changes get synced in editors that use buffers, like Vim and IntelliJ? Need to write plugins for them?
- Figure out the algorithm for how changes get applied. Woot, Treedoc, Logoot, or Operational Transforms?

## Later ideas
- Turn server into a hybrid p2p model, whereby clients connect to the server to attain an initial list of peers.
- Kite server is run as another p2p node, to provide availability should all clients disconnect.
- Secure p2p network with some kind of signing mechanism (probably SSL client certs?)
- Build out our own VCS, and stop relying to git/svn/mercurial.
   - Interfacing with git/mercurial needs to be easy to gather users.
   - Can PR type comments/conversation be stored in metadata alongside the files? That way, we don't need a separate PR commenting system. Maybe something like [Pottery, by Nat Pryce?](https://github.com/npryce/pottery)
   - Support groups of people writing code, not just one author.
- Allow users to configure their own transport.
- IPFS as a transport layer?

## References
- [Operational Transform Lua code, used by floobits](https://github.com/Floobits/diffshipper/blob/master/src/lua/diff_match_patch.lua)
- [IntelliJ floobits](https://github.com/Floobits/floobits-intellij)

## Message Types

- Peers, and topic subscriptions / Connect / Disconnect
- Updates

### Connect

    {
      "request": {
        "subscribe_topics": [
          "gitlab.com/cjwebb/kite/master"
        ]
      }
    }

    {
      "modify": {
        "peers": {
          "address": "127.0.0.1",
          "topics": [
            "gitlab.com/cjwebb/kite/master"
          ]
        }
      }
    }

### Get information about peers.

    {
      "request": {
        "info": [
          "peers"
        ]
      }
    }

    {
      "peers": [
        {
          "address": "127.0.0.1:V4",
          "topics": [
            "gitlab.com/cjwebb/kite/master"
          ]
        }
      ]
    }

### Send and receive modifications

    {
      ???
    }
