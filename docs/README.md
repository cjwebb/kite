# Documentation

The Kite documentation lives here.

## Iteration Goals

Here we define stages of development, as it is currently envisaged. This is likely to change as development progresses and we learn things.

### 0 - MVP
The goal of this iteration is to get a working tool, which syncs file system changes between a central server and any connected clients.

Kite Client:
 - will need to poll, or receive events, for file system changes.
 - will construct diffs by parsing `git diff`.
 - will keep track of what has already been sent over the client's kite connection, and only send deltas.

Kite Server:
 - will broadcast to all clients any received (well-formed) messages.
 - handle client connections, and disconnections.

### 1 - TBD
### 2 - TDB