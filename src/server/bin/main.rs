#[macro_use]
extern crate serde_derive;
extern crate serde_json;

use std::sync::mpsc;
use serde_json::Error;
use std::collections::HashMap;
use std::io::prelude::*;
use std::net::{SocketAddr, TcpListener, TcpStream, Shutdown};
use std::sync::{Arc, Mutex};
use std::thread;
use std::io::{BufReader, BufWriter}; // todo - use these

#[derive(Serialize, Deserialize, Debug, Clone)]
struct Message {
    msg: String,
}

type Tx = mpsc::Sender<Message>;
type Rx = mpsc::Receiver<Message>;
type SharedState = Arc<Mutex<HashMap<SocketAddr, Tx>>>;

/// Basic architecture requires two threads per connection, one to read, and one to write.
/// Reading communicates to writing threads via mpsc channels, which are stored in shared state.
fn main() {
    let listener = TcpListener::bind("127.0.0.1:12345").unwrap();
    let state = Arc::new(Mutex::new(HashMap::new()));
    println!("Kite server running on port 12345");

    for stream in listener.incoming() {
        let stream = stream.unwrap();
        setup_peer(stream, &state);
    }
}

fn setup_peer(write_stream: TcpStream, shared_state: &SharedState) {
    let addr = write_stream.peer_addr().unwrap();
    let (tx, rx) = mpsc::channel();
    println!("Establishing peer {:?}", addr);

    let state = Arc::clone(shared_state);
    state.lock().unwrap().insert(addr, tx);
    let read_stream = write_stream.try_clone().unwrap();

    thread::spawn(move || {
        handle_write(write_stream, rx);
    });

    thread::spawn(move || {
        handle_read(read_stream, state);
    });
}

fn handle_read(stream: TcpStream, state: Arc<Mutex<HashMap<SocketAddr, Tx>>>) {
    let stream_addr = stream.peer_addr().unwrap();
    loop {
        match read_message(&stream) {
            Ok(message) => {
                for (addr, tx) in state.lock().unwrap().iter() {
                    if *addr != stream_addr {
                        tx.send(message.clone()).unwrap();
                    }
                }
                println!("Received a message from client {:?}", message);
            }
            Err(e) => {
                println!("Read error {:?}", e);
                state.lock().unwrap().remove(&stream_addr);
                break;
            }
        }
    }
    println!("Reader thread closing");
    stream.shutdown(Shutdown::Both);
}

fn handle_write(mut stream: TcpStream, rx: Rx) {
    loop {
        match rx.recv() {
            Ok(message) => {
                println!("Received a message from peer {:?}", message);
                let json = format!("{}\n", serde_json::to_string(&message).unwrap());
                stream.write(json.as_bytes()).unwrap();
                stream.flush().unwrap();
            }
            Err(e) => {
                println!("Write error {:?}", e);
                break;
            }
        }
    }
    println!("Writer thread closing");
}

// todo - How do we handle json errors from `stream`?
fn read_message(mut stream: &TcpStream) -> Result<Message, Error> {
    let mut buf = vec![0; 256]; // todo - make this better, so it doesn't error on large messages
    stream.read(&mut buf); // todo - propogate this error
    buf.retain(|&x| x != 0); // remove the trailing zeroes, so serde works
    serde_json::from_slice(&buf)
}
