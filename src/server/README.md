# Kite Server

    kite-server --help

## Todo
- Subscriptions => Allow clients to specify which topics they are interested in (use the git-url & branch as the topic name)
- Require clients provide a name, and subscriptions they want in an initial handshake.

## Usage without an IntelliJ plugin
This could still listen to the filesystem, and send `git diff` changes on the change-stream. No caret information would be available, but we would be able to sync across machines.

### Todo: FileSystem changes
- Parse git diff messages into a datastructure.
- Keep track of what has been sent, in memory... and reconcile diffs against it. Hopefully that would make the CRDT converge.
