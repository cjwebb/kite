extern crate clap;

use clap::{App, Arg, SubCommand};

fn main() {
    let matches = App::new("Kite")
        .version("0.0.1")
        .author("Kickstand Consulting Ltd.")
        .about("A modern version control system")
        .subcommand(
            SubCommand::with_name("graft")
                .about("Connect your branch to the stream of changes.")
                .arg(Arg::with_name("branch")),
        ).subcommand(
            SubCommand::with_name("cut") // maybe rename to "ignore"?
                    .about("Stop changes being applied to branch(es). Defaults to $CURRENT_BRANCH")
                    .arg(Arg::with_name("branch")
                        .short("b")
                        .index(1)
                        .multiple(true)
                        .required(true)
                        .default_value("$CURRENT_BRANCH")),
        ).get_matches();

    if let Some(_matches) = matches.value_of("init") {
        println!("Init")
    } else {
        println!("What?");
    }

    // spawn/kill background job to listen to stream of changes.
    // read this thread: https://www.reddit.com/r/rust/comments/5v6eab/how_to_run_a_process_in_the_background/
}
